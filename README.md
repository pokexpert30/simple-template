# Simple cicd template

Contains the regular gitlab.com example CI file, and a variable pre-set named hello. This template is used to demonstrate https://gitlab.com/gitlab-org/gitlab/-/issues/441771 by being included in https://gitlab.com/pokexpert30/simple-template-user ci file.
